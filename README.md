**Datetime methods for Salesforce Flows **


*Introduction*

At the moment Datetime and Business Hours are not particularly well supported in Salesforce Flows.  The intent of this code is to be a gap filler until such time as Salesforce delivers more functionality.  The way this code works is simple - Salesforce provides much more support in Apex for Datetime manipulation and Business Hours calcs, so the class included here simply serves to provide access to a subset of those Apex Datetime, Date and Business Hour class methods. 48 in total so there is something there for everyone.
While some of these functions are probably possible using formulas, in a lot of cases here you can manipulate the result into different timezones and formats - not something I'd personally want to be doing with formulas if there is a ready built capability to be found on the platform.


---

## Datetime support

The Datetime capabilities available to Salesforce Developers can be found [here](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_methods_system_datetime.htm).  

The following subset of capabilities or 'methods' provided by Salesforce are enabled in Flow through this simple class. To call Datetime specific methods prefix the method with '**Datetime.**'. For example, to use the Datetime addDays method use the methodname attribute 'Datetime.addDays'.

* addDays
* addHours
* addMinutes
* addMonths
* addSeconds
* addYears
* day
* dayGmt
* format
* formatGmt
* formatLong
* getTime
* hour
* hourGmt
* millisecond
* millisecondGmt
* minute
* minuteGmt
* month
* monthGmt
* now (exists in Flow however I included this to enable formatting and timezone controls)
* second
* secondGmt
* time
* timeGmt
* year
* yearGmt


## Business Hour support

In addition it made sense to extend this class to also provide five methods for Business Hours calculations.

The Business Hours capabilities enabled are described in more detail [here](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_classes_businesshours.htm).

To access these methods from the action, prefix the method you want to use with '**BusinessHours.**'. For example, to use the diff method you'd populate the action attribute 'methodName' with 'BusinessHours.diff'.

* add (add business hours time to a starting datetime)
* addGmt (add business hours time to a starting datetime and return in Gmt)
* diff (return difference in milliseconds between two datetimes, only considering business hours)
* isWithin (confirm if a datetime is within business hours)
* nextStartDate (confirm next closest date that is within business hours)

## Date support

Was on a role by this stage so figured best to expose the Date Class methods as well.

More information on Date Class methods can be found [here](https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_methods_system_date.htm).

To access the following Date Class methods, prefix the method you want to use with '**Date.**'. For example, to use the dayOfWeek method you'd populate the action attribute 'methodName' with 'Date.dayOfWeek'.

* addDays
* addMonths
* addYears
* day
* dayOfYear
* daysBetween
* daysInMonth
* format
* isLeapYear
* isSameDay
* month
* monthsBetween
* today
* toStartOfMonth
* toStartOfWeek
* year

---

## Installation

The code base comprises two classes - an invocable method named 'DateTimeUtility' and an accompanying test class. You can copy the code to your org manually, clone this repo and add to a project or install both options via [this unmanaged package](https://login.salesforce.com/packaging/installPackage.apexp?p0=04t5g00000095Xj&isdtp=p1). Its totally up to you. 


## Using the classes from within Flow

Once installed just reference the DatetimeUtility or DateUtility class from within Flow Builder Action to access Datetime and BusinessHours methods, or DateUtility class from a Flow Action to access the Date methods.  

Once selected, you'll want to configure some attributes. There are 3 to 4 attributes to populate depending on which method you wish to call. If its not intuitive enough let me know and I'll expand the documentation.

## Utility Method attributes

The following attributes can be configured once you've created the action using either of the DatetimeUtility or DateUtility Classes

- **methodName**. This is mandatory and if not valid the logic will error out with an invalid method name error at runtime.  The method names used in the class are exactly as they're documented in the formal Salesforce documentation, just without the brackets. So where Salesforce calls a method 'addDays()' you'd just use 'addDays'.
- **dateTimeString**. This is mandatory and is the Flow datetime variable you want to manipulate. Typically from a datetime input field for example. The format of a Flow datetime string is like this '1/1/2021, 3:47 pm' or the pattern 'dd/mm/yyyy, hh:mm:ss a'.
- **increment**. This is populated with a number when using the addSeconds, addDays etc.  Any method where you're needing to pass in a numerical value.
- **returnFormat**. This is optional on all method calls, allowing you to manipulate the format of the returned result.  The expectation is that you're passing in a string describing a java simple date format pattern. See more on this [here](https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html). 
- **timeZone**. This is optional on all methods and assuming you provide a [valid timezone code](https://www.javacodex.com/Date-and-Time/List-All-TimeZones) you can have the resulting datetime value returned to you in the timezone provided.
- **secondDateString**. This is only used when performing a BusinessHours.diff calculation. The method will thrown a runtime error if this second datetime is not provided.

## Response attributes

The Utility returns a response that has a number of attributes you can use as you wish

- **errorOccured**. This is a boolean that if true means something went wrong during processing.
- **errorMessage**. If errorOccured is true, then there is likely a message provided in this attribute to describe what it was that went wrong.
- **result**. This is a string containing the result of the method call. Its worth noting that to keep this consistent ALL results are in string format, including what would normally be expected as an integer or long datatype i.e. the result from 'day' or 'time' method calls. Sorry about that but I figured trying to accommodate different data types in the result might confuse more than it'd help. Can extend the result class easily enough if you'd prefer more result attributes.
